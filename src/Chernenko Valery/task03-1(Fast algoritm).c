#include <stdio.h>
#include <time.h>
#define NUM 8

int main()
{
	int i,j,nums,num_of_bits=0;
	long long int sum_of_numbers[NUM][9*NUM+1];
	clock_t start, finish;
	double time[NUM];

	printf("Enter number of bits in range [1..%d] >> ",NUM);
	scanf("%d",&num_of_bits);
	if (num_of_bits>NUM || num_of_bits<1)
	{
		printf("Sorry, Incorret data entry!!! \n");
		return 0;
	}
	start=clock();
	for (j=0;j<=NUM-1;j++)
		for (i=0;i<=9*NUM;i++)
			sum_of_numbers[j][i]=0;
	for (i=0;i<=9;i++)
		sum_of_numbers[0][i]=1;
	for(nums=2;nums<=num_of_bits;nums++)
	{
		for (i=0;i<=9*nums;i++)
			sum_of_numbers[nums-1][i]=sum_of_numbers[nums-2][i];
		for (i=1;i<=9;i++)
			for (j=0;j<=9*nums;j++)
				sum_of_numbers[nums-1][j+i]+=sum_of_numbers[nums-2][j];
	}
	finish=clock();
	time[num_of_bits-1]=(double)(finish-start)/CLOCKS_PER_SEC;
	printf("Sum for %d bits:\n\n", num_of_bits);
	for (i=0;i<=9*num_of_bits;i++)
		printf("Sum of numbers for %d is %lld\n",i, sum_of_numbers[num_of_bits-1][i]);
	puts("");
	printf("Cost time: %.2f sec\n",time[num_of_bits-1]);
	return 0;
}