#define N 10 
#define MINI -100 
#define MAXI 100 
#include <stdio.h> 
#include <stdlib.h> 
#include <time.h> 
void minimum(int a, long *min) 
{ 
	if (*min > a) 
	*min = a; 
} 
void maximum(int a, long *max) 
{ 
	if (*max < a) 
	*max = a; 
} 
int average(int a, double sr) 
{ 
	return a + sr; 
} 
int main() 
{ 
	int a[N], i; 
	long min = MAXI + 1, max = MINI - 1; 
	double sr = 0; 
	srand(time(0)); 
	for (i = 0; i < N; i++) 
	{ 
		a[i] = rand() % (MAXI - MINI) + MINI; 
		minimum(a[i], &min); 
		maximum(a[i], &max); 
		sr = average(a[i], sr); 
	} 
	sr = sr / N; 
	printf("max=%i\n", max); 
	printf("min=%i\n", min); 
	printf("average=%.3f\n", sr); 
	return 0; 
}