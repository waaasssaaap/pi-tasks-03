#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define size 10
#define random 10 // ������ ���������� ������ ������� 0 <= x < random

void min_max_med( int *arr,int *min,int *max,double *med)
{
	*min = arr[0];
	*max = arr[0];
	*med = arr[0];
	for (int i = 0; i < size; i++)
	{
		if (arr[i] < *min)
			*min = arr[i];
		if (arr[i] > *max)
			*max = arr[i];
		*med = (*med + arr[i]) / 2;
	}
}

int main()
{
	int int_arr[size] = {1,2};
	int min, max;
	double med;
	srand(time(0));
	for (int i = 0; i < size; i++) //���������� ������� ������ �������
	{
		int_arr[i] = rand() % random;
	}
	min_max_med(int_arr,&min,&max,&med);
	printf("Minimum:%d ; Maximum:%d ; Medium:%f\n", min, max, med);
	return 0;
}