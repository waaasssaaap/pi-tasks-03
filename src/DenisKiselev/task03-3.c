#include <stdio.h>
#include <string.h>
#define N 127
int main()
{
	int sym[N],i;
	char str[256];
	printf("Enter a line: ");
	fgets(str,256,stdin);
	for (i=0; i<N; i++)
		sym[i]=0;
	for (i=0; i<strlen(str)-1; i++)
		sym[str[i]]++;
	for (i=0; i<N; i++)
		if (sym[i]!=0)
			printf("%c-%d\n",i,sym[i]);
	return 0;
}
