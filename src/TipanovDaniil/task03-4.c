#define N 50
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void arrGen(int arr[N]) //��� ������� ������ void arrGen(int*arr, int N) ������, ������?
{
	for (int i = 0; i < N; i++)
	{
		arr[i] = rand();
		printf("%d", arr[i]);
		putchar((i + 1) % 5 != 0 ? '\t' : '\n');
	}
	printf("\n");
}

void min_max_avr(int arr[N])
{
	int min = arr[0], max = arr[0];
	float avr = 0;
	for (int i = 0; i < N; i++)
	{
		avr += arr[i];
		if (arr[i] < min) min = arr[i];
		if (arr[i] > max) max = arr[i];
	}
	avr /= N;
	printf("Min: %d\tMax: %d\tAverage: %f\n", min, max, avr);
}

int main()
{
	int arr[N];
	srand(time(0));
	printf("Now random array[50] will be generated and you'll get min, max and avr!\n");
	arrGen(arr);
	min_max_avr(arr);
	return 0;
}