#define _CRT_SECURE_NO_WARNINGS
#define N 73
#include <stdio.h>
#include <time.h>

void numbersSum(int *array, int size)
{
	clock_t start, finish;
	int numLengh, currentNum;
	double t;
	long i, num = 0, sum;
	long long int digitsSum[N] = { 0 };
	printf("Enter the number (1 - 8): ");
	scanf("%d", &numLengh);
	if (numLengh <= 0 || numLengh >= 9)
	{
		printf("Input error!\n");
		return 1;
	}
	start = clock();
	for (i = 0; i < numLengh; i++)
		num = num * 10 + 9;
	for (i = 0; i <= num; i++)
	{
		sum = 0;
		currentNum = i;
		while (currentNum)
		{
			sum += (currentNum % 10);
			currentNum /= 10;
		}
		digitsSum[sum] += 1;
	}
	finish = clock();
	t = (finish - start) / CLOCKS_PER_SEC;
	for (i = 0; i <= numLengh * 9; i++)
		printf("%d %lld\n", i, digitsSum[i]);
	printf("Wasted time: %.1f sec\n", t);
	return 0;
}

int main()
{
	long long int digitsSum[N];
	numbersSum(digitsSum, N);
	return 0;
}