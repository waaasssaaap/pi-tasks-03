#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
int NumOfNines(int digit)
{
	int i, result = 0;
	for(i = 0; i < digit; i++)
	{
		result = result*10 + 9;
	}
	return result;
}
int SumOfDigits(int num)
{
	int sum = 0;
	while(num > 0)
	{
		sum += num % 10;
		num = num / 10;
	}
	return sum;
}
int main()
{
	int NumOfDigits, interval, range, i, j;
	double start, finish, t;
	start = clock();
	printf("Enter the number of digits in range of 1 - 8\n");
	scanf("%d", &NumOfDigits);
	if((NumOfDigits < 1) || (NumOfDigits > 8))
	{
		printf("Error 404\n");
		return 1;
	}	
	range = NumOfDigits*9 + 1;
	interval = NumOfNines(NumOfDigits);
	int buf[256];
	for(i = 0; i < range; i++)
	{
		buf[i] = 0;
		for(j = 0; j <= interval; j++)
		{
			if(i == SumOfDigits(j))
				buf[i]++;
		}
	}
	for(i = 0; i < range; i++)
		printf("%d\t%d\n", i,buf[i]);
	finish = clock();
	t = finish - start;
	printf("Time = %f\n", t);
	return 0;
}