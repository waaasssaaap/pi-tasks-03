#include <stdio.h>
#include <time.h>
#define NUM 8

int main()
{
	int i,j,num=0,sum,buf,num_of_bits=0;
	long long int sum_of_numbers[9*NUM+1]={(long long int)0};
	clock_t start, finish;
	double time;
	
	printf("Enter bits your number in [1..8] >> ");
	if(scanf("%d",&num_of_bits)==0 || num_of_bits>8 || num_of_bits<0) 
		{
			puts("sorry, incorrect data entry");
			return ;
		}
	start=clock();
	for (i=0;i<num_of_bits;i++)
		num=num*10+9;
	for (i=0;i<=num;i++)
	{
		sum=0;
		buf=i;
		while (buf)
		{
			sum+=(buf%10);
			buf/=10;
		}
		sum_of_numbers[sum]+=1;
	}
	finish=clock();
	time=(double)finish-start/CLOCKS_PER_SEC;
	for (i=0;i<=num_of_bits*9;i++)
	printf("Sum of numbers for %d is %lld\n",i, sum_of_numbers[i]);
	puts("");
	printf("Cost time: %.2f sec\n",time);
	return 0;
}