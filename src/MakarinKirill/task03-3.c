#include <stdio.h>
#define N 256

int main()
{
	char str[N]; // ������� ������. 
	int qtSym[26] = {0}; // ���-�� �������� ��������.
	int i = 0, strLength = 0;

	fgets(str, N, stdin);

	while (str[i] != '\n')
	{
		qtSym[(int)str[i] - 97]++;
		i++;
	}

	for (i = 0; i < 26; i++)
	{
		if(qtSym[i] != 0)
			printf(" %c - %d ", i+97, qtSym[i] );
	}
	printf("\n");

	return 0;
}