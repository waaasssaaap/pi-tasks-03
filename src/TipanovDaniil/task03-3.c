#include <stdio.h>
int main()
{
	char str[256];
	int symb[128];
	int k = 0, stroka = 0;
	printf("Enter a line: ");
	fgets(str, 256, stdin);
	for (int i = 0; i < 128; i++)
	{
		for (int j = 0; str[j] != '\n'; j++)
		{
			if (str[j] == i)
				k++;
		}
		if (k != 0)
		{
			printf("%c-%d", i, k);
			stroka++;
			putchar(stroka % 4 != 0 ? '\t' : '\n');
		}
		k = 0;
	}
	printf("\n");
	return 0;
}