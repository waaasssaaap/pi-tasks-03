#define RANDOM_NUMBER 10 
#define N 100
#include <stdio.h>
#include <time.h>

int main(){
	unsigned int arr[N];
	int maxLength = 0;
	int primaryLength = 0;
	int maxNumberofLength;
	int i ;
	srand(time(0));
	for (i=0; i < N; i++){
		arr[i] = rand() % RANDOM_NUMBER;
		printf("%i, ", arr[i]);
	}
	for (i=1; i < N; i++){
		if (arr[i] == arr[i-1]){
			if (primaryLength == 0)
				primaryLength = 2;
			else
				primaryLength++;
		}
		
		else {
			if (maxLength < primaryLength){
				maxLength = primaryLength;
				maxNumberofLength = arr[i - 1];
			}
			primaryLength = 0;
		}
	}
	
	if (maxLength > 0){
		printf("\nMaxLength = %d\n", maxLength);
		printf("follower numbers:");
		for (i=0; i < maxLength-1 ; i++){
			printf("%d, ", maxNumberofLength);
		}
		printf("%d\n", maxNumberofLength);
	}
	return 0;
}