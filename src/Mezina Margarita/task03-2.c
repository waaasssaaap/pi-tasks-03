#define N 100
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main()
{
	srand(time(0));
	int i, a[100];
	int h = -1, max = 0, t=0;
	for (i = 0; i < N; ++i) {
		a[i] = rand() % 10;
		if (a[i] == h)
			++t;
		else
		{
			h = a[i];
			t = 1;
		}
		if (t > max)
			max = t;
		printf("%i", a[i]);
		if (i != N - 1)
			printf(",");
	}
	printf("\n");
	printf("%i", max);
	return 0;
}