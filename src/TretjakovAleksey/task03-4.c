#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void minimum(int i, int *min)
{
	if (i < *min)
		*min = i;
}
void maximum(int i, int *max)
{
	if (i > *max)
		*max = i;
}
double average(int j)
{
	double sr = j / 25.0;
	printf("average=%f\n", sr);
}
int main()
{
	srand(time(NULL));
	int max = -101, min = 101;
	int line[25];
	double sr = 0;
	for (int i = 0; i < 25; i++)
	{
		line[i] = rand() % (100 - (-100)) + (-100);
		minimum(line[i], &min);
		maximum(line[i], &max);
		sr += line[i];
		printf("%i ", line[i]);
	}
	puts("");
	printf("max=%i\n", max);
	printf("min=%i\n", min);
	average(sr);
	return 0;
}