#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define N 10
#define R 500
int i, max = 0, min = 500;

int Min(int *arr)
 {
	int min = 500;
	for (i = 0; i<N; i++)
	 {
		if (arr[i]<min)
			 min = arr[i];
	 }
	return min;
   }

int Max(int *arr)
 {
	int max = 0;
	for (i = 0; i<N; i++)
		 {
		if (arr[i]>max)
		 max = arr[i];
	      }
	return max;
	}

float Avg(int *arr)
 {
	float avg = .0f;
	for (i = 0; i<N; i++)
		 avg += arr[i];
	avg /= N;
	return avg;
	}

int main()
 {
	int arr[N];
	srand(time(0));
	for (i = 0; i<N; i++)
		 arr[i] = rand() % R;
	printf("Minimum = %d\nMaximum = %d\nAverage = %.2f\n", Min(arr), Max(arr), Avg(arr));
	return 0;
	}