#include <stdio.h>
#include <time.h>
#include <math.h>

int sum(int n)
{
        int k = 0, c = n;
	
	while(c>0){
		k+=c % 10;
		c/=10;
	}
	
	return k;	
}


int sumValid[73];
       
int main()
{       
	
	clock_t startTest, finishTest, startProgram, finishProgram;
	int t = 1, validN, Ncicle, optimization = 0;                              	
	double secTest, secProgram;
                 
	startProgram = clock();

	while(t<9){

		for(int i = 0; i<73; i++) sumValid[i] = 0;	          
                 
		startTest = clock();
		
		validN = t*9;
        	Ncicle = 9;
	
		for(int i = 0; i<t-1;i++) Ncicle = Ncicle*10 +9;

		optimization = ceil((double)Ncicle/2);
		
		for(int i = 0; i<=optimization; i++) sumValid[sum(i)]++, sumValid[validN-sum(i)]++;
                                                                                          	
		finishTest = clock();
		  
		secTest = (double) (finishTest-startTest) / CLOCKS_PER_SEC;

 		printf("\n For numb[1..%d] and sums[0..%d] time is %.4f seconds \n",Ncicle,validN,secTest);		
		
		t++;	
	}

	finishProgram = clock();

	secProgram = (double) (finishProgram - startProgram) / CLOCKS_PER_SEC;
	
	printf("\n Program for 9 intervals has been finished in %.2f seconds \n", secProgram);	                                                            


	return 0;
		        
}                      