#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
	int number, length = 1, maxlength = 0, i;
	char sequence = 0;
	char buf[256];
	printf("Enter the number of symbols: \n");
	scanf("%d", &number);
	srand(time(0));
	buf[0] = rand() %9 + '1'; //��������� ������� �������� ���� ����� �� ������ ��������� (�� 1 �� 9), ��� ��� �� ������ ����� �� ����� ������ 0 
	for(i = 1; i < number; i++)
	{
		buf[i] = rand() %10 + '0'; //��������� ������� �������� ���� ����� ����� �� 10 ��������� (�� 0 �� 9)
	}
	for(i = 0; i < number - 1; i++)
	{
		if(buf[i] == buf[i+1])
		{
			length += 1;
			if(length >= maxlength)
			{
				maxlength = length;
				sequence = buf[i];
			}
		}
		else 
		{
			length = 1;
		}
	}
	for(i = 0; i < number; i++)
	{
		printf("%c", buf[i]);
	}
	printf("\nSequence = ");
	if(maxlength == 0)
		printf("%d", sequence);
	else
	{
		for(i = 0; i < maxlength; i++)
			printf("%c ", sequence);
	}
	printf("\nLength = %d\n", maxlength);
	return 0;
}