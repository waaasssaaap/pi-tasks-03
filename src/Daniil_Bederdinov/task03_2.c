#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
    int str[256] = { 0 };
    int len = 0;
    int count = 1;
    int max = 0;
    int seq = -1;
    srand(time(NULL));
    printf("Enter length of your line : ");
    scanf("%i", &len);
    for (int i = 0; i < len; i++)
    {
        str[i] = rand() % 10;
        if (str[i] == seq)
            ++count;
        else
        {
            seq = str[i];
            count = 1;
        }
        if (count > max)
            max = count;
        printf("%i", str[i]);
    }
    puts("");
    printf("%i \n", max);
    return 0;
}