#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <time.h>
#define N 10

//���������� ������� ���������� �������.
void randArr(int *arr)
{
	for (int i = 0; i < N; i++)
		arr[i] = rand()%41-20;
}

// ����� �������.
void outArr(int *arr)
{
	for (int i = 0; i < N; i++)
		printf("%d ", arr[i]);
	printf("\n");
}

//����� �������������.
int maxInArr(int *arr)
{
	int max = INT_MIN;
	for (int i = 0; i < N; i++)
		if (arr[i] >= max)
			max = arr[i];
	return max;
}

// ����� ������������.
int minInArr(int *arr)
{
	int min = INT_MAX;
	for (int i = 0; i < N; i++)
		if (arr[i] <= min)
			min = arr[i];
	return min;
}

// ���������� ��. ���������������.
double midSum(int *arr)
{
	int sum = 0;
	for (int i = 0; i < N; i++)
		sum += arr[i];
	return ((double)sum/N);
}


int main()
{
	int arr[N];

	srand(time(0));

	randArr(arr);
	outArr(arr);

	printf("Max = %d \t Min = %d \t MidlleSum = %f \n", maxInArr(arr), minInArr(arr), midSum(arr));

	return 0;
}
