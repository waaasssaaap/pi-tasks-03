#include <stdio.h>
#include <string.h>
#define N 256
#define MAXCHAR 128

void clearArr(int *arr,int size)
{
    int i;
    for(i=0;i<size;i++)
    {
        arr[i]=0;
    }
}
int isStrCorrect(char * str, int size)
{
    int i;
    for (i=0;i<size;i++)
    {
        if (str[i]<0) return 0;
    }
    return 1;
}

void printTable(int*table,int size)
{
    int i =0;
    for(i=0;i<size;i++)
    {
        if (table[i]>0)
        {
            printf("%c - %d \n",(char)i,table[i]);
        }
    }
}

int main(void)
{
    signed char str[N];
    int table[MAXCHAR];
    printf("Enter a string: ");
    clearArr(table,MAXCHAR);
    fgets(str,N,stdin);
    str[strlen(str)-1]='\0';
    if (!isStrCorrect(str, strlen(str)))
    {
        printf("ERROR");
        return 1;
    }
    int i=0;
    while (str[i])
    {
        table[(int)str[i]]++;
        i++;
    }
    printTable(table,MAXCHAR);
    return 0;
}

