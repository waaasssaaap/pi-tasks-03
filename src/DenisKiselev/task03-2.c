#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20
int main ()
{
	int arr[N], i, qty=1, value, max=1; //qty-quantity
	srand(time(0));
	printf("Random array: ");
	arr[0]=rand()%10;
	for (i=1; i<N; i++)
	{
		arr[i]=rand()%10;
		if (arr[i]==arr[i-1])
		{
			qty++;
			if (qty>=max)
			{
				value=arr[i];
				max=qty;
			}
		}
		else
			qty=1;
		printf("%d ",arr[i]);
	}
	if (max==1)
		printf("\nNope sequence of repetitive elements!\n");
	else
	{
		printf("\nLength: %d",max);
		printf("\nSequence: ");
		for (i=0; i<max; i++)
			printf("%d ",value);
	}
	return 0;
}
